<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Email;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class EmailController
 * @package App\Controller
 */
class EmailController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $emails = $this->getDoctrine()
            ->getRepository(Email::class)
            ->findAll();

        return $this->render('email/index.html.twig', [
            'emails' => $emails,
        ]);
    }

    /**
     * @Route("/email/new", name="new")
     */
    public function new(Request $request)
    {
        $email = new Email();
        $email->setDate(new \DateTime());

        $categories = $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();

        $category_tab = [];
        foreach ( $categories as $category) {
            $category_tab[$category->getName()] = $category;
        }

        $form = $this->createFormBuilder($email)
            ->add('sender', TextType::class, array('label' => 'De', 'attr' => array('class' => 'form-control')))
            ->add('recipient', TextType::class, array('label' => 'À', 'attr' => array('class' => 'form-control')))
            ->add('subject', TextType::class, array('label' => 'Sujet', 'attr' => array('class' => 'form-control')))
            ->add('category', ChoiceType::class, array('choices' => $category_tab, 'attr' => array('class' => 'form-control')))
            ->add('message', TextareaType::class, array('label' => 'Message', 'attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Envoyer', 'attr' => array('class' => 'form-control')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('email/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/email/{id}", name="showbyid")
     */
    public function showById($id)
    {
        $mail = $this->getDoctrine()->getManager()->getRepository(Email::class)->find($id);

        return $this->render('email/showbyid.html.twig', [
            'mail' => $mail
        ]);
    }

    /**
     * @Route("/email/category/new", name="newcategory")
     */
    public function newCategory(Request $request)
    {
        $category = new Category();

        $form = $this->createFormBuilder($category)
            ->add('name', TextType::class, array('label' => 'Nom'))
            ->add('save', SubmitType::class, array('label' => 'Créer'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('index');
        }

        return $this->render('email/category.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/email/category/{param}", name="email_category_id")
     */
    public function showByCategory($param)
    {
        $categories = $this->getDoctrine()->getManager()->getRepository(Email::class)->find($param);

        return $this->render('email/showbyid.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/email/delete/{id}", name="delete_email")
     */
    public function deleteEmail($id)
    {
        $em = $this->getDoctrine()->getManager();
        $email = $em->getRepository(Email::class)->find($id);

        $em->remove($email);
        $em->flush();
    }
}
